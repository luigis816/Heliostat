/* 
Name        = Coleostat control

Author      = Luigi Stahl

Version     = 5.2.0.0

Depedencies = TinyGPS++.h
              SoftwareSerial.h
              rgb_lcd.h 
              
*/ 
//bibliotheques
#include <TinyGPS++.h>
#include <SoftwareSerial.h>
#include "rgb_lcd.h"

//Serial logiciels
SoftwareSerial MotorSerialHor(10, 11); // RX, TX
SoftwareSerial MotorSerialVer(12, 13);

//Interface homme machine
#define BPV 2
#define BPU 3
rgb_lcd lcd;
bool JoyControl = LOW;
long PotVert = 0;
int MovVert = 0;
long PotHor = 0;
int MovHor = 0;
const int HorOffset = 20;
const int VerOffset = 40;


//Variables de calcul
#define DEG_TO_RAD 0.01745329 
#define PI 3.141592654 
#define TWOPI 6.28318531
float T,JD_frac,L0,M,e,C,L_true,f,R,GrHrAngle,Obl,RA,Decl,HrAngle,elev,azimuth;   
long JD_whole,JDx;
float Lon=7.45*DEG_TO_RAD, Lat=48*DEG_TO_RAD;
int mirrorAngleDeg = 0;
float UpdHoriz;
float UpdVerti;
int zone=0; //GPS ==> heure solaire

//Variables de communication inter arduino
#define InterSensor 4
bool reception = LOW;
bool getting = LOW;
float OldElev = 0, OldAzi = 0;
int sensorValue = 0; 
int GetValue = 0;
bool valueOK = LOW;


//variables de débogage
const int analogInPin = 0;
const int AnalogHorOffset = 1;
const int AnalogVerOffset = 2;
const int AnalogHor = 3;
const int AnalogVert = 2;

//Variables de fonctionnement
int dir = 2;
int mode;
bool user;
int timer = 0;


//Variables GPS
static const uint32_t GPSBaud = 9600;
TinyGPSPlus gps;


void setup() {
  //broches d'interruptions
  attachInterrupt(digitalPinToInterrupt(BPV), boutonR, CHANGE);
  attachInterrupt(digitalPinToInterrupt(BPU), boutonJ, HIGH);
  pinMode(BPV, INPUT);
  pinMode(BPU, INPUT);
  //initialisation de l'écran
  lcd.begin(16, 2);
  lcd.setRGB(255, 0, 0);
  //initialisation du serial a but de débogage
  Serial.begin(9600);
  //initialisation du serial de communication avec le module GPS
  Serial2.begin(9600);
  //initialisation des serials de communication avec l'arduino-moteur
  MotorSerialHor.begin(9600);
  MotorSerialVer.begin(9600);
  //phase d'initialisation
  while(millis()<180000){
    //reglage par l'utilisateur a l'aide du Joystick
    if(millis()>120000){
      Joystick();
    } else {
      lcd.setCursor(0,0);
      lcd.print("initialisation");
    }
  }
  //reglage a l'aide de l'arduino-capteur
  while(!valueOK){
    GetData();
  }
} 

//Fonctions d'interruptions de debug
void boutonR(){
  reception = !reception;
  if(getting){
    getting=!getting;
  }
}

void boutonJ(){
  JoyControl = !JoyControl;
  if(getting){
    getting=!getting;
  }
}

//fonction de calcul de déplacement du miroir 
void MirrorCalc(float Elev, float Azi, float OldElev, float OldAzi){
  float deplaHor, deplaVert;
  deplaHor = (Azi-OldAzi)/2;
  deplaVert = (Elev-OldElev)/2;
  MotorControl(deplaHor, deplaVert);
}

//fonction d'envoie d'information à l'arduino-moteur
void MotorControl(float Horiz, float Verti){
  if(Horiz!=0)MotorSerialHor.write(Horiz);if(Verti!=0)MotorSerialVer.print(Verti);
}
/*
void SolarScan(){
  
}
*/

//fonction du controle utilisateur
void Joystick(){
  //lecture et traitement de la position horizontale du Joystick
  PotHor = analogRead(AnalogHor)+HorOffset;
  Serial.println(PotHor);
  MovHor = map(PotHor, 0, 1023, 0, 4);
  //lecture et traitement de la position verticale du Joystick
  PotVert = analogRead(AnalogVert)+VerOffset;
  Serial.println(PotVert);
  MovVert = map(PotVert, 0, 1023, 0, 4);
  //gestion de l'information de mouvement et du l'affichage sur l'IHM
  lcd.setCursor(0, 0);
  switch(MovHor){
    case 0:
      lcd.print("Gauche+ ");
      MotorControl(-1, 0);
      break;
    case 1:
      lcd.print("Gauche  ");
      MotorControl(-0.01, 0);
      break;
    case 2:
      if(MovVert==2){
        lcd.print("Immobile ");
      }else{
        lcd.print("        ");
      }
      break;
    case 3:
      lcd.print("Droite  ");
      MotorControl(0.01, 0);
      break;
    case 4:
      lcd.print("Droite+ ");
      MotorControl(1, 0);
      break;
  }
  lcd.setCursor(9, 0);
  switch(MovVert){
    case 4:
      lcd.print("Bas+   ");
      MotorControl(0, -1);
      break;
    case 3:
      lcd.print("Bas    ");
      MotorControl(0, -0.01);
      break;
    case 2:
      lcd.print("       ");
      break;
    case 1:
      lcd.print("Haut   ");
      MotorControl(0, 0.01);
      break;
    case 0:
      lcd.print("Haut+  ");
      MotorControl(0, 1);
      break;
  }
}

//fonction de reception a partir de l'arduino-capteur
void GetData(){
  //lecture du serial correspondant à l'arduino-capteur
  if (Serial3.available()) {
    GetValue = Serial3.read();
  }
  //gestion de l'information de mouvement et de l'affichage sur l'IHM
  lcd.setCursor(8, 1);
  Serial.println(GetValue);
  switch(GetValue){
    case 0:
      lcd.print("Attend   ");
      break;
    case 1:
      lcd.print("Haut    ");
      MotorControl(0, -0.01);
      break;
    case 2:
      lcd.print("Bas     ");
      MotorControl(0, 0.01);
      break;
    case 3:
      lcd.print("Erreur");
      break;
    case 4:
      lcd.print("Gauche  ");
      MotorControl(0.01, 0);
      break;
    case 5:
      lcd.print("Haut/Gau");
      MotorControl(0.01, -0.01);
      break;
    case 6:
      lcd.print("Bas/Gauc");
      MotorControl(0.01, 0.01);
      break;
    case 7:
      lcd.print("Ha/Ba/Ga");
      MotorControl(0.01, 0);
      break;
    case 8:
      lcd.print("Droite  ");
      MotorControl(0.01, 0);
      break;
    case 9:
      lcd.print("Haut/dro");
      MotorControl(-0.01, -0.01);
      break;
    case 10:
      lcd.print("Bas/droi");
      MotorControl(-0.01, 0.01);
      break;
    case 11:
      lcd.print("Ha/Ba/Dr");
      MotorControl(-0.01, 0);
      break;
    case 12:
      lcd.print("Erreur");
      break;
    case 13:
      lcd.print("Ha/ga/dr ");
      MotorControl(0.01, 0);
      break;
    case 14:
      lcd.print("Bas gauche droite");
      MotorControl(0, 0.01);
      break;
    case 15:
      lcd.print("Ok");
      valueOK = HIGH;
      return;
      break;
  } 
}

//fonction de calcul de la date julienne
long JulianDate(int year, int month, int day) {   
  long JD_whole;   
  int A,B;   
  if (month<=2) {      
    year--; month+=12;   
  }     
  A=year/100; 
  B=2-A+A/4;   
  JD_whole=(long)(365.25*(year+4716))+(int)(30.6001*(month+1))+day+B-1524;   
  return JD_whole; 
}  

//fonction delay utilisée dans un but de lecture de la trame GPS
static void smartDelay(unsigned long ms){
  unsigned long start = millis();
  do 
  {
    while (Serial2.available())
      gps.encode(Serial2.read());
  } while (millis() - start < ms);
}



//routine de fonctionnement
void loop() {
  //aiguillage calculs ou capteur
  switch(mode){
    case 1:
    //aiguillage Joystick ou arduino-capteur
      switch(user){
        case 0:
          if(!getting){
            lcd.setRGB(255, 0, 255);
            lcd.clear();
            getting=!getting;
          }
          Joystick();
        case 1:
          if(!getting){
            lcd.setRGB(255, 255, 0);
            lcd.clear();
            lcd.print("Recept des donnees");
            lcd.setCursor(0, 1);
            lcd.print("Zones :");
            getting=!getting;
          }
          GetData();
        //si variable mode incohérente
        default:
          mode = !mode;
      }
    //fonctions propres a la bibliotheque d'analyse NMEA
    default:
      static const double LONDON_LAT = 51.508131, LONDON_LON = -0.128002;

      unsigned long distanceKmToLondon =
        (unsigned long)TinyGPSPlus::distanceBetween(
          gps.location.lat(),
          gps.location.lng(),
          LONDON_LAT, 
          LONDON_LON) / 1000;
    
      double courseToLondon =
        TinyGPSPlus::courseTo(
          gps.location.lat(),
          gps.location.lng(),
          LONDON_LAT, 
          LONDON_LON);
  
    
      const char *cardinalToLondon = TinyGPSPlus::cardinal(courseToLondon);
    
      Serial.println();
      

  //Reception de la trame GPS
      smartDelay(100);
  //Calculs astronimiques
      JD_whole=JulianDate(gps.date.year(),gps.date.month(),gps.date.day());       
      JD_frac=(gps.time.hour()+gps.time.minute()/60.+gps.time.second()/3600.)/24.-.5;       
      T=JD_whole-2451545; T=(T+JD_frac)/36525.;      
      L0=DEG_TO_RAD*fmod(280.46645+36000.76983*T,360);       
      M=DEG_TO_RAD*fmod(357.5291+35999.0503*T,360);       
      e=0.016708617-0.000042037*T;       
      C=DEG_TO_RAD*((1.9146-0.004847*T)*sin(M)+(0.019993-0.000101*T)*sin(2*M)+0.00029*sin(3*M));       
      f=M+C; 
      Obl=DEG_TO_RAD*(23+26/60.+21.448/3600.-46.815/3600*T);            
      JDx=JD_whole-2451545;         
      GrHrAngle=280.46061837+(360*JDx)%360+.98564736629*JDx+360.98564736629*JD_frac;       
      GrHrAngle=fmod(GrHrAngle,360.);           
      L_true=fmod(C+L0,TWOPI);       
      R=1.000001018*(1-e*e)/(1+e*cos(f));       
      RA=atan2(sin(L_true)*cos(Obl),cos(L_true));       
      Decl=asin(sin(Obl)*sin(L_true));       
      HrAngle=DEG_TO_RAD*GrHrAngle+Lon-RA;       
      elev=asin(sin(Lat)*sin(Decl)+cos(Lat)*(cos(Decl)*cos(HrAngle)));       
      //Azimut mesuré par l'est depuis le nord       
      azimuth=PI+atan2(sin(HrAngle),cos(HrAngle)*sin(Lat)-tan(Decl)*cos(Lat));    
  //Inscriptions des résultats des calculs dans le serial de débogage
      Serial.print(gps.date.year()); Serial.print("/"); Serial.print(gps.date.month());       
      Serial.print("/"); Serial.print(gps.date.day()); Serial.print(", ");        
      Serial.print(gps.time.hour()); Serial.print("h");       
      Serial.print(gps.time.minute()); Serial.print("min"); Serial.print(gps.time.second()); Serial.print("sec");             
      Serial.print(","); Serial.print(JD_whole);        
      Serial.print(","); Serial.print(JD_frac,7);       
      Serial.print(","); Serial.print("Elevation="); Serial.print(elev/DEG_TO_RAD,3);        
      Serial.print(","); Serial.print("Azimut="); Serial.print(azimuth/DEG_TO_RAD,3); Serial.println();
  //inscriptions des résultats des calculs sur l'IHM
      lcd.clear();
      lcd.setRGB(200, 200, 180);
      lcd.setCursor(0, 0);
      lcd.print("Elev="); lcd.print(elev/DEG_TO_RAD,3);
      lcd.setCursor(0, 1);
      lcd.print("Azimuth="); lcd.print(azimuth/DEG_TO_RAD,3);
  //Exploitation des résultats.
      MirrorCalc(elev, azimuth, OldElev, OldAzi);
      OldElev = elev/DEG_TO_RAD;
      OldAzi = azimuth/DEG_TO_RAD;
  }
}

